import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('mango1.jpg')
img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

list_mango = []
for i in range(len(img)):
    for j in range(len(img[i])):
        if thresh[i,j] != 0:
            list_mango.append(img[i,j])

list_R = []
list_G = []
list_B = []

for i in list_mango:
    list_R.append(i[0])
    list_G.append(i[1])
    list_B.append(i[2])

from statistics import mode
R = mode(list_R)
G = mode(list_G)
B = mode(list_B)

list_rgb = [R,G,B]

def rgb2lab ( inputColor ) :

    num = 0
    RGB = [0, 0, 0]

    for value in inputColor :
        value = float(value) / 255

        if value > 0.04045 :
               value = ( ( value + 0.055 ) / 1.055 ) ** 2.4
        else :
            value = value / 12.92

        RGB[num] = value * 100
        num = num + 1

    XYZ = [0, 0, 0,]

    X = RGB [0] * 0.4124 + RGB [1] * 0.3576 + RGB [2] * 0.1805
    Y = RGB [0] * 0.2126 + RGB [1] * 0.7152 + RGB [2] * 0.0722
    Z = RGB [0] * 0.0193 + RGB [1] * 0.1192 + RGB [2] * 0.9505
    XYZ[ 0 ] = round( X, 4 )
    XYZ[ 1 ] = round( Y, 4 )
    XYZ[ 2 ] = round( Z, 4 )

    XYZ[ 0 ] = float( XYZ[ 0 ] ) / 95.047        
    XYZ[ 1 ] = float( XYZ[ 1 ] ) / 100.0          
    XYZ[ 2 ] = float( XYZ[ 2 ] ) / 108.883        
    
    num = 0
    for value in XYZ :

        if value > 0.008856 :
            value = value ** ( 0.3333333333333333 )
        else :
            value = ( 7.787 * value ) + ( 16 / 116 )

        XYZ[num] = value
        num = num + 1

    Lab = [0, 0, 0]

    L = ( 116 * XYZ[ 1 ] ) - 16
    a = 500 * ( XYZ[ 0 ] - XYZ[ 1 ] )
    b = 200 * ( XYZ[ 1 ] - XYZ[ 2 ] )

    Lab [ 0 ] = round( L, 4 )
    Lab [ 1 ] = round( a, 4 )
    Lab [ 2 ] = round( b, 4 )

    return Lab

list_rgb = [R,G,B]

def howold(x) :
    l,a,b = x[0],x[1],x[2]
    periods = [[52,-14,27],[52,-13,28],[53,-11,31],[54,-12,32],[55,-10,34],[55,-10,38],[58,-10,40],[59,-7,41],[60,-6,42.5],[61,-5,44],[62,3,46],[64,8,47],[66,9,48],[68,10.5,48],[68,14,50],[68,14.5,52],[70,17.5,51],[71,18,51],[71,20,51],[73,21.5,51],[72,20,51],[70,18,51],[69,16,50],[68,16,50],[64,16,49]]
    shortest = 99999999
    for i in range(len(periods)):
        dist = ((l-periods[i][0])**2+(a-periods[i][1])**2+(b-periods[i][2])**2)
        if shortest > dist:
            result = i+1
            shortest = dist
    return result

print(rgb2lab(list_rgb))
print(howold(rgb2lab(list_rgb)))